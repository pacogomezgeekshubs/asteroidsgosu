require "gosu"
require "./Bala"

class Nave
    def initialize(window)
        #Centramos la nave al medio de la pantalla
        @x = window.width/2
        @xTop=@x
        @y = window.height/2
        @yTop=@y
        @window=window
        @sprite = Gosu::Image.new(window, "./media/nave.png", true)
        #Inicializamos a nulo variables que después utilizaremos
        @angleToTurn=0
        #balas
        @balas=[]
        #prevent multiple balas
        @balasTime=0
    end
    #Seguir al raton
    def followMouse
        xDistance = @window.mouse_x - @x;
        yDistance = @window.mouse_y - @y;
        angleToTurnRad=Math.atan2(yDistance, xDistance)
        @angleToTurn=angleToTurnRad*180/Math::PI
        @angleToTurn=@angleToTurn+90
        #calculating top
        @xTop=@x+Math.cos(angleToTurnRad)*21
        @yTop=@y+Math.sin(angleToTurnRad)*21

    end
    def draw()
        @sprite.draw_rot(@x, @y, 0,@angleToTurn)
        @balas.each do |bala|
            bala.draw
        end
    end
    def update()
        followMouse
        if @balasTime>0
            @balasTime -=1
        end
        if @window.button_down? Gosu::MS_LEFT
            if @balasTime ==0
                @balas.push(Bala.new(@window,@xTop,@yTop,@window.mouse_x,@window.mouse_y))
                @balasTime=10
            end
        end
        @balas.each do |bala|
            if bala.x > 640
                @balas.delete(bala)
            elsif bala.x < 0
                @balas.delete(bala)
            elsif bala.y > 640
                @balas.delete(bala)
            elsif bala.x < 0
                @balas.delete(bala)
            else
                bala.update
            end
        end
    end
    
end