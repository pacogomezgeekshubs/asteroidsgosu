require "gosu"

class Bala
    attr_reader :x,:y

    def initialize(window,x,y,targetx,targety)
        @x = x
        @inicialX=x
        @y = y
        @inicialY=y
        @target_x = targetx
        @target_y = targety
        @speed=5
        @distance=21
        @angleToTurnRad=angle()
        @window=window
        @sprite = Gosu::Image.new(window, "./media/bala.png", true)
    end
    def angle()
        xDistance = @target_x - @x;
        yDistance = @target_y - @y;
        @angleToTurnRad=Math.atan2(yDistance, xDistance)
    end
    def draw()
        @sprite.draw(@x, @y, 0)
    end
    def update()
        #calculating line
        @distance=@distance+@speed
        @x=@inicialX+Math.cos(@angleToTurnRad)*@distance
        @y=@inicialY+Math.sin(@angleToTurnRad)*@distance
    end
end