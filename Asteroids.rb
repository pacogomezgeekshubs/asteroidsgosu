require "gosu"
require "./Nave"

class Asteroids <Gosu::Window
    def initialize    
        super 640, 640, false
        self.caption = "ASTEROIDS"
        #background
        @bg=Gosu::Image.new(self, "./media/bg.png", true)
        #nave
        @nave = Nave.new(self)
        #music
        @music = Gosu::Song.new("./media/bensound-moose.mp3")
        #@music.play(true)
    end
    def draw
        @bg.draw(0,0,0)
        @nave.draw
    end
    def update
        @nave.update
    end
    def needs_cursor?
        true
    end
end